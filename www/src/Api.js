import axios from 'axios'
import { BASE_URL, STORAGE_KEY } from './consts'

export const Api = () => {
  let token = "F07VwN0vLZLgquRvYjt0pXRWWYUXefxBmKweCRYbnJqVXdBuJP6XxFLDIpBS"

  return axios.create({
    baseURL: BASE_URL,
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json'
    }
  })
}
