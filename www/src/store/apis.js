import { Api } from '../Api'

export default {
    userList () {
    return Api().get('/difficulty')
  },
 
  siteContentsList () {
    return Api().get('/site/contents/app')
  },
  siteCategoryList () {
    return Api().get('/category/list')
  },
  
  categoryWiseContent (param) {
    return Api().get('/category-contents?title='+param)
  }
}