import apis from '../apis'
import { LocalStorage } from 'quasar'
import { STORAGE_KEY } from '../../consts'

export const usersListed = ({ commit }, payload) => {
    return apis.userList().then((res) => {
        console.log('data_store',res)
      commit('getAllUsers', res.data.data)
    })
  }

  export const siteContent = ({ commit }, payload) => {
    return apis.siteContentsList().then((res) => {
        console.log('siteContentsList',res.data)
      commit('siteContentsList', res.data)
    })
  }
  export const siteCategoryList = ({ commit }, payload) => {
    return apis.siteCategoryList().then((res) => {
        console.log('siteCategoryList',res.data)
      commit('SiteCategoryList', res.data)
    })
  }
  
  export const categoryWiseContent = ({ commit }, payload) => {
    console.log('Payload action:= '+payload);
    return apis.categoryWiseContent(payload).then((res) => {
        console.log('categoryWiseContent',res.data)
      commit('CategoryWiseContentList', res.data)
    })
  }