export const getUserAll = (state) => {
    return state.userList
  }
export const siteContents = (state) => {
    return state.siteContents
  }
export const siteCategories = (state) => {
    return state.siteCategories
  }
export const categoryContents = (state) => {
    return state.categoryContents
  }